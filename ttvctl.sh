#!/bin/bash

setup() {
/sbin/ifconfig
sudo ip link set dev p4p1 up 
sudo ip addr add fd16:abcd:ef05:2::2/64 dev p4p1
ssh pi@fd16:abcd:ef05:2::1 "sudo radvd"
sudo ip route add fd16:abcd:ef05:3::/64 via fd16:abcd:ef05:2::1

}
sniffer() {

ssh pi@fd16:abcd:ef05:2::1 'sudo dumpcap -P -i lowpan0 -w -' | wireshark -k -i -

}
setupRPL() {
	
sudo ip addr add fd16:abcd:ef01:1::05/64 dev eth0
sudo ip route add fd16:abcd:ef01:3::/64 via fd16:abcd:ef01:1::1
ssh pi@fd16:abcd:ef05:2::1 "sudo systemctl stop lowpan
sudo systemctl start lowpan_monitor"
ping6 -s 1 -c 1 fd16:abcd:ef01:3:d1c1:6d43:ab5d:1336 

}
snifferRPL() {

ssh pi@fd16:abcd:ef05:2::1 'sudo dumpcap -P -i monitor0 -w -' | wireshark -k -i -

}
pingRPL() {
out = $(ping6 -s 1 -c 1 fd16:abcd:ef01:3:d1c1:6d43:ab5d:1336 ping6 -s 1 -c 1 fd16:abcd:ef01:3:d1c1:6d43:ab5d:1336)
echo $out
}
