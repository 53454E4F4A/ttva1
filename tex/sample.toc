\select@language {english}
\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Aufgabenstellung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Hardware}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Sensor}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Raspberry Pi}{1}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Workstation}{1}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Netzwerkaufbau}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Szenario 1}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Szenario 2}{2}{subsection.1.2.2}
\contentsline {chapter}{\numberline {2}Laboreinbindung von Gateways und Sensorknoten (Szenario 1)}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Neighbor Discovery Optimization}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Durchf\"uhrung}{3}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Konfiguration}{3}{subsection.2.2.1}
\contentsline {subsubsection}{\nonumberline Sensor}{3}{section*.3}
\contentsline {subsubsection}{\nonumberline RPi}{3}{section*.4}
\contentsline {subsubsection}{\nonumberline Workstation}{3}{section*.5}
\contentsline {subsection}{\numberline {2.2.2}Capture}{4}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Analyse}{4}{section.2.3}
\contentsline {chapter}{\numberline {3}RPL Routing im Labornetz (Szenario 2)}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}RPL Routing}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Durchf\"uhrung}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Analyse}{7}{section.3.3}
\contentsline {chapter}{\numberline {4}Datenverteilung und Messung (Szenario 2)}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Constrained Application Protocol (CoAP)}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}Durchf\"uhrung}{8}{section.4.2}
\contentsline {section}{\numberline {4.3}Analyse}{9}{section.4.3}
